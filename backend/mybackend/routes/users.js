const express = require('express');
const User = require('../controller/conectdb');
const userController = require('../controller/UserController');
const router = express.Router();
const UserController = require('../controller/UserController');
/* GET users listing. */


router.get('/', userController.getUsers)
router.get('/:id', userController.getUser)
router.post('/', userController.addUser)
router.put('/', userController.updateUser)
router.delete('/:id', userController.deleteUser)    
 



  
  




module.exports = router;
