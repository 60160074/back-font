const User = require('../controller/conectdb');
const userController = {
  userlist: [
    {
      id: 1,
      name: "sorawit",
      gender: "M",
      birthday: "26/04/2541",
      education: "ปริญาตรี",
      position: "dev",
    },
    {
      id: 2,
      name: "gorawit",
      gender: "M",
      birthday: "26/04/2541",
      education: "ปริญาตรี",
      position: "dev",
    },
  ],
  lastid: 2,


  async addUser(req, res, next){
    const payload = req.body
    const users  = new User(payload)
    try {
     await users.save()
     res.json(users)
        } catch (err) {
          res.status(500).send(err)
        }
  
  
  },
  async updateUser(req, res, next){
    const payload = req.body

  try {
    await User.updateOne({_id: payload._id},payload)
    res.json("update")
   
    res.json(users)
       } catch (err) {
         res.status(500).send(err)
       }
  
  
  },
  async deleteUser(req, res, next){
    const {id} = req.params

  //res.json(UserController.delelteUser(payload))
  try {
    await User.deleteOne({_id:id})
   
    res.json("delete")
       } catch (err) {
         res.status(500).send(err)
       }
  },
  
  
  async getUsers(req, res, next){
    try {
      const users = await User.find({})
      res.json(users)
    } catch {
      res.status(500).send(err)
    }
  
  
  },
  async getUser(req, res, next){
    const { id } = req.params
    try {
  const users  = await User.findById(id)
  res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  
  
  }


}
module.exports = userController