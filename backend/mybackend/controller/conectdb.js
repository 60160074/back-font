const mongoose  = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://admin01:60160074@localhost/mydb',{useNewUrlParser:true})
const db = mongoose.connection

db.on('error',console.error.bind(console,'connection error:'))
db.once('open',function(){
    console.log('connect')
})

const userSchema =  new  Schema({
    name: String,
    gender: String,
    birthday: String,
    education: String,
    position: String

})

const User = mongoose.model('User',userSchema)

User.find(function(err, users){
    if(err) return console.log(err)
    console.log(users)
})
module.exports = User